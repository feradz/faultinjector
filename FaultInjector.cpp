/*
 * FaultInjector.h
 *
 *  Created on: August 6, 2014
 *      Author: Ferad Zyulkyarov
 *       Email: ferad.zyulkyarov@bsc.es
 *
 * This is a PIN tool which injects errors into the address space of
 * the application which is being instrumented.
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include "pin.H"

//
// I add these includes because eclipse's indexer cannot find them
// and shows some types and declarations from these files as an error.
//
//#include "xed-iclass-enum.h"
//#include "ins_api_xed_ia32.PH"
//#include "fund.hpp"

using namespace std;


ofstream outf;
KNOB<string> KnobOutputFule(KNOB_MODE_WRITEONCE, "pintool", "o", "memtrace.out", "specify output file name");
bool IsAnalysisEnabled = false;
bool IsInstrumentationEnabled = false;
unsigned long long MagicInstructionCount = 0;

const string FaultInjectorApi_MemoryRegisterStart = "FaultInjectorApi_MemoryRegisterStart";
const string FaultInjectorApi_MemoryRegisterEnd = "FaultInjectorApi_MemoryRegisterEnd";
const string FaultInjectorApi_MemoryRegister = "FaultInjectorApi_MemoryRegister";

/**
 * This method is celled whenever a magic instruction is encountered.
 *
 * This method is instrumented whenever a magic instruction fnop is executed.
 * In the executable should define and call the following function at the
 * places when the pin tool should be toggled between enabled and disabled
 * states.
 *
 * static inline InsertMagicInstruction() __attribute__((always_inline))
 * {
 *     asm volatile ("fnop");
 * }
 *
 * @return
 */
VOID PIN_FAST_ANALYSIS_CALL ToggleMagicInstruction()
{
	IsAnalysisEnabled = !IsAnalysisEnabled;
}

/**
 * Load instruction has been detected.
 * This is the call back function which is called when a load
 * instruction is encountered.
 *
 * @param ip the instruction pointer.
 * @param addr the memory address which is loaded.
 * @param length number of bytes loaded (read).
 */
VOID LoadCallback(THREADID tid, VOID * ip, VOID * addr, UINT32 length)
{
	if (!IsAnalysisEnabled)
	{
		return;
	}
	outf << ip << ": R " << addr << endl;
}

/**
 * Store instruction has been detected.
 * This is the call back function which is called when a store
 * instruction is encountered.
 *
 * @param ip the instruction pointer (program counter).
 * @param addr the address which is stored.
 * @param length number of bytes stored (written).
 */
VOID StoreCallback(THREADID tid, VOID * ip, VOID * addr, UINT32 length)
{
	if (!IsAnalysisEnabled)
	{
		return;
	}
	outf << ip << ": W " << addr << endl;
}

VOID FaultInjectorApi_MemoryRegisterCallback(ADDRINT startAddr, unsigned long long numBytes)
{
	outf << "Memory register: " << hex << startAddr << " : " << dec << numBytes << endl;

	//
	// Modify the memory
	//
	int modifiedValue = 77;
	int *addr = (int*)startAddr;
	int originalValue;
	PIN_SafeCopy(&originalValue, addr, sizeof(originalValue));
	PIN_SafeCopy(addr, &modifiedValue, sizeof(modifiedValue));
	cout << "Original value: " << originalValue << ", Modified value:" << modifiedValue << endl;
}

/**
 * Instruments a call to the ToggleMagicInstruction if the passed
 * instruction is a magic instruction.
 *
 * This method is instrumented whenever a magic instruction fnop is executed.
 * In the executable should define and call the following function at the
 * places when the pin tool should be toggled between enabled and disabled
 * states.
 *
 * static inline void InsertMagicInstruction() __attribute__((always_inline))
 * {
 *     asm volatile ("fnop");
 * }
 *
 * @param ins the instruction to be potentially instrumented.
 * @return true if instrumented and false if not.
 */
bool InstrumentCallToToggleMagicInstruction(INS ins)
{
	if (INS_Opcode(ins) == XED_ICLASS_FNOP)
	{
		MagicInstructionCount++;
		INS_InsertCall(
			ins,
			IPOINT_BEFORE,
			AFUNPTR(ToggleMagicInstruction),
			IARG_FAST_ANALYSIS_CALL,
			IARG_END);

		IsInstrumentationEnabled = !IsInstrumentationEnabled;
	}

	return IsInstrumentationEnabled;
}

/**
 * Function called by pin to instrument an instruction.
 *
 * @param ins the instruction to be instrumented.
 * @param ip the instruction pointer (program counter).
 * @param insBytes the size of the instruction in bytes.
 * @return
 */
VOID InstrumentInstruction(INS ins, ADDRINT ip, unsigned int insBytes)
{
	if (!IsInstrumentationEnabled)
	{
		return;
	}

	//
	// Instrument the loads
	//
	if (INS_IsMemoryRead(ins))
	{
		INS_InsertCall(
			ins,
			IPOINT_BEFORE,
			(AFUNPTR) LoadCallback,
			IARG_THREAD_ID,
			IARG_INST_PTR,
			IARG_MEMORYREAD_EA,
			IARG_MEMORYREAD_SIZE,
			IARG_END);
	}

	if (INS_HasMemoryRead2(ins))
	{
		INS_InsertCall(
			ins,
			IPOINT_BEFORE,
			(AFUNPTR) LoadCallback,
			IARG_THREAD_ID,
			IARG_INST_PTR,
			IARG_MEMORYREAD2_EA,
			IARG_MEMORYREAD_SIZE,
			IARG_END);
	}

	//
	// Instrument the stores
	//
	if (INS_IsMemoryWrite(ins))
	{
		INS_InsertCall(
			ins,
			IPOINT_BEFORE,
			(AFUNPTR) StoreCallback,
			IARG_THREAD_ID,
			IARG_INST_PTR,
			IARG_MEMORYWRITE_EA,
			IARG_MEMORYWRITE_SIZE,
			IARG_END);
	}
}

/**
 * Function called by pin to instrument a trace of instructions (this is a pin trace).
 *
 * @param trace the trace to be instrumented.
 * @return
 */
VOID InstrumentTrace(TRACE trace, VOID *v)
{
	ADDRINT ip = TRACE_Address(trace);
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		const INS head = BBL_InsHead(bbl);

		if (!INS_Valid(head))
		{
			continue;
		}

		for(INS ins = head; INS_Valid(ins); ins = INS_Next(ins))
		{
			unsigned int instructionSize = INS_Size(ins);
			InstrumentCallToToggleMagicInstruction(ins);
			InstrumentInstruction(ins, ip, instructionSize);
			ip += instructionSize;
		}
	}
}

/**
 * Instruments the calls to the callback methods for
 * the memory register functions.
 *
 * @param img a binary image
 * @param v
 * @return
 */
VOID InstrumentImage(IMG img, VOID *v)
{
	//
	// Find the memory register function.
	//
	RTN memoryRegisterRtn = RTN_FindByName(img, FaultInjectorApi_MemoryRegister.c_str());
	if (RTN_Valid(memoryRegisterRtn))
	{

		RTN_Open(memoryRegisterRtn);

		//
		// Instrument the call to obtain the arguments
		//
		RTN_InsertCall(
			memoryRegisterRtn,
			IPOINT_BEFORE,
			(AFUNPTR) FaultInjectorApi_MemoryRegisterCallback,
			IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
			IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
			IARG_END);

		RTN_Close(memoryRegisterRtn);
	}
}

VOID Fini(INT32 code, VOID *v)
{
    outf << "#eof" << endl;
    outf.close();
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    PIN_ERROR( "This Pintool injects faults into the address space of the application which is being instrumented.\n"
              + KNOB_BASE::StringKnobSummary() + "\n");
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */

int main(int argc, char *argv[])
{
	//
	// Initialize pin and the symbol manager
	//
	PIN_InitSymbols();
    if (PIN_Init(argc, argv)) return Usage();

    outf.open(KnobOutputFule.Value().c_str());

    IMG_AddInstrumentFunction(InstrumentImage, 0);
    TRACE_AddInstrumentFunction(InstrumentTrace, 0);
    PIN_AddFiniFunction(Fini, 0);

    // Never returns
    PIN_StartProgram();

    return 0;
}
